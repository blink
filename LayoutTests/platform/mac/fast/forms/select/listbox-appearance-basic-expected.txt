layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x551
  LayoutBlockFlow {HTML} at (0,0) size 800x551
    LayoutBlockFlow {BODY} at (8,8) size 784x535
      LayoutText {#text} at (42,49) size 5x18
        text run at (42,49) width 5: " "
      LayoutText {#text} at (88,49) size 5x18
        text run at (88,49) width 5: " "
      LayoutText {#text} at (0,0) size 0x0
      LayoutText {#text} at (134,49) size 5x18
        text run at (134,49) width 5: " "
      LayoutText {#text} at (196,49) size 5x18
        text run at (196,49) width 5: " "
      LayoutText {#text} at (248,49) size 5x18
        text run at (248,49) width 5: " "
      LayoutText {#text} at (301,49) size 5x18
        text run at (301,49) width 5: " "
      LayoutText {#text} at (353,49) size 5x18
        text run at (353,49) width 5: " "
      LayoutBR {BR} at (357,49) size 1x18
      LayoutText {#text} at (46,120) size 5x18
        text run at (46,120) width 5: " "
      LayoutText {#text} at (92,120) size 5x18
        text run at (92,120) width 5: " "
      LayoutBR {BR} at (96,120) size 1x18
      LayoutText {#text} at (42,187) size 5x18
        text run at (42,187) width 5: " "
      LayoutBR {BR} at (46,187) size 1x18
      LayoutText {#text} at (42,254) size 5x18
        text run at (42,254) width 5: " "
      LayoutBR {BR} at (46,254) size 1x18
      LayoutText {#text} at (48,383) size 5x18
        text run at (48,383) width 5: " "
      LayoutText {#text} at (106,383) size 5x18
        text run at (106,383) width 5: " "
      LayoutText {#text} at (170,383) size 5x18
        text run at (170,383) width 5: " "
      LayoutText {#text} at (233,383) size 5x18
        text run at (233,383) width 5: " "
      LayoutBR {BR} at (0,0) size 0x0
      LayoutText {#text} at (55,517) size 5x18
        text run at (55,517) width 5: " "
      LayoutText {#text} at (130,517) size 5x18
        text run at (130,517) width 5: " "
      LayoutBR {BR} at (134,517) size 1x18
layer at (12,12) size 34x59 clip at (13,13) size 21x57 scrollHeight 56
  LayoutListBox {SELECT} at (4,4.25) size 34.06x58.75 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 21.06x14.19
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "foo"
layer at (58,12) size 34x59 clip at (59,13) size 21x57 scrollHeight 56
  LayoutListBox {SELECT} at (50.06,4.25) size 34.27x58.75 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 21.27x14.19 [color=#FFFFFF] [bgcolor=#0069D9]
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "foo"
    LayoutBlockFlow {OPTION} at (1,15.19) size 21.27x14.19
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "bar"
layer at (104,12) size 35x59 clip at (105,13) size 22x57 scrollHeight 56
  LayoutListBox {SELECT} at (96.33,4.25) size 34.27x58.75 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 21.27x14.19 [bgcolor=#D4D4D4]
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "foo"
    LayoutBlockFlow {OPTION} at (1,15.19) size 21.27x14.19 [color=#808080] [bgcolor=#D4D4D4]
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "bar"
layer at (151,12) size 49x59 clip at (152,13) size 36x57 scrollHeight 56
  LayoutListBox {SELECT} at (142.59,4.25) size 49.55x58.75 [color=#7F7F7F] [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 36.55x14.19 [color=#808080] [bgcolor=#D4D4D4]
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "foo"
    LayoutBlockFlow {OPTION} at (1,15.19) size 36.55x14.19
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "bar"
    LayoutBlockFlow {OPTGROUP} at (1,29.38) size 36.55x28.38
      LayoutBlockFlow {DIV} at (0,0) size 36.55x14.19
      LayoutBlockFlow {OPTION} at (0,14.19) size 36.55x14.19 [color=#808080] [bgcolor=#D4D4D4]
        LayoutInline {<pseudo:before>} at (0,0) size 14x13
          LayoutTextFragment (anonymous) at (2,0) size 14x13
            text run at (2,0) width 14: "    "
        LayoutText {#text} at (15,0) size 20x13
          text run at (15,0) width 20: "baz"
layer at (212,55) size 41x16 clip at (213,56) size 28x14 scrollHeight 56
  LayoutListBox {SELECT} at (204.14,46.81) size 40.58x16.19 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo1"
    LayoutBlockFlow {OPTION} at (1,15.19) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo2"
    LayoutBlockFlow {OPTION} at (1,29.38) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo3"
    LayoutBlockFlow {OPTION} at (1,43.56) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo4"
layer at (265,41) size 40x30 clip at (266,42) size 27x28 scrollHeight 56
  LayoutListBox {SELECT} at (256.72,32.63) size 40.58x30.38 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo1"
    LayoutBlockFlow {OPTION} at (1,15.19) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo2"
    LayoutBlockFlow {OPTION} at (1,29.38) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo3"
    LayoutBlockFlow {OPTION} at (1,43.56) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo4"
layer at (317,26) size 41x45 clip at (318,27) size 28x43 scrollHeight 56
  LayoutListBox {SELECT} at (309.30,18.44) size 40.58x44.56 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo1"
    LayoutBlockFlow {OPTION} at (1,15.19) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo2"
    LayoutBlockFlow {OPTION} at (1,29.38) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo3"
    LayoutBlockFlow {OPTION} at (1,43.56) size 27.58x14.19
      LayoutText {#text} at (2,0) size 24x13
        text run at (2,0) width 24: "foo4"
layer at (12,79) size 38x63 clip at (15,82) size 21x57 scrollHeight 56
  LayoutListBox {SELECT} at (4,71.25) size 38.06x62.75 [bgcolor=#FFFFFF] [border: (3px solid #00FF00)]
    LayoutBlockFlow {OPTION} at (3,3) size 21.06x14.19
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "foo"
layer at (62,83) size 34x59 clip at (63,84) size 21x57 scrollHeight 56
  LayoutListBox {SELECT} at (54.06,75.25) size 34.06x58.75 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 21.06x14.19
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "foo"
layer at (12,150) size 34x59 clip at (13,151) size 21x57 scrollHeight 56
  LayoutListBox {SELECT} at (4,142.25) size 34.06x58.75 [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 21.06x14.19
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "foo"
layer at (12,217) size 34x59 clip at (13,218) size 21x57 scrollHeight 56
  LayoutListBox {SELECT} at (4,209.25) size 34.06x58.75 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 21.06x14.19
      LayoutText {#text} at (2,0) size 18x13
        text run at (2,0) width 18: "foo"
layer at (12,322) size 41x83 clip at (13,323) size 28x81 scrollHeight 80
  LayoutListBox {SELECT} at (4,314.25) size 40.88x82.75 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 27.88x20.19
      LayoutText {#text} at (2,0) size 24x18
        text run at (2,0) width 24: "foo"
layer at (65,303) size 46x102 clip at (66,304) size 33x100
  LayoutListBox {SELECT} at (56.88,295) size 45.88x102 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 32.88x25
      LayoutText {#text} at (2,0) size 29x23
        text run at (2,0) width 29: "foo"
layer at (123,284) size 51x121 clip at (124,285) size 38x119
  LayoutListBox {SELECT} at (114.75,275.81) size 51.34x121.19 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 38.34x29.80
      LayoutText {#text} at (2,0) size 35x28
        text run at (2,0) width 35: "foo"
layer at (186,322) size 52x83 clip at (187,323) size 39x81 scrollHeight 80
  LayoutListBox {SELECT} at (178.09,314.25) size 51.81x82.75 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 38.81x20.19
      LayoutText {#text} at (2,0) size 24x18
        text run at (2,0) width 24: "foo"
    LayoutBlockFlow {OPTION} at (1,21.19) size 38.81x29.80
      LayoutText {#text} at (2,0) size 35x28
        text run at (2,0) width 35: "bar"
layer at (14,450) size 44x87 clip at (15,451) size 31x85
  LayoutListBox {SELECT} at (6,441.81) size 43.58x87.19 [bgcolor=#FFFFFF] [border: (1px solid #999999)]
    LayoutBlockFlow {OPTION} at (1,1) size 30.58x21.30
      LayoutText {#text} at (3,0) size 25x19
        text run at (3,0) width 25: "foo"
layer at (76,417) size 54x118 clip at (78,419) size 39x114 scrollHeight 113
  LayoutListBox {SELECT} at (67.58,409.44) size 54.63x117.56 [bgcolor=#FFFFFF] [border: (2px solid #999999)]
    LayoutBlockFlow {OPTION} at (2,2) size 39.63x28.39
      LayoutText {#text} at (4,0) size 32x26
        text run at (4,0) width 32: "foo"
