CONSOLE WARNING: SVG's SMIL animations (<animate>, <set>, etc.) are deprecated and will be removed. Please use CSS animations or Web animations instead.

SVG SMIL:
PASS: maskContentUnits from [userSpaceOnUse] to [objectBoundingBox] was [userSpaceOnUse] at 0
PASS: maskContentUnits from [userSpaceOnUse] to [objectBoundingBox] was [userSpaceOnUse] at 0.2
PASS: maskContentUnits from [userSpaceOnUse] to [objectBoundingBox] was [objectBoundingBox] at 0.6
PASS: maskContentUnits from [userSpaceOnUse] to [objectBoundingBox] was [objectBoundingBox] at 1

Web Animations API:
PASS: maskContentUnits from [userSpaceOnUse] to [objectBoundingBox] was [userSpaceOnUse] at -2.4
PASS: maskContentUnits from [userSpaceOnUse] to [objectBoundingBox] was [userSpaceOnUse] at 0
PASS: maskContentUnits from [userSpaceOnUse] to [objectBoundingBox] was [userSpaceOnUse] at 0.2
PASS: maskContentUnits from [userSpaceOnUse] to [objectBoundingBox] was [objectBoundingBox] at 0.6
PASS: maskContentUnits from [userSpaceOnUse] to [objectBoundingBox] was [objectBoundingBox] at 1
PASS: maskContentUnits from [userSpaceOnUse] to [objectBoundingBox] was [objectBoundingBox] at 3.4

