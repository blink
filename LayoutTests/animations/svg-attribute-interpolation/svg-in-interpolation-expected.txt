CONSOLE WARNING: SVG's SMIL animations (<animate>, <set>, etc.) are deprecated and will be removed. Please use CSS animations or Web animations instead.

SVG SMIL:
PASS: in from [SourceAlpha] to [FillPaint] was [SourceAlpha] at 0
PASS: in from [SourceAlpha] to [FillPaint] was [SourceAlpha] at 0.2
PASS: in from [SourceAlpha] to [FillPaint] was [FillPaint] at 0.6
PASS: in from [SourceAlpha] to [FillPaint] was [FillPaint] at 1
PASS: in2 from [BackgroundImage] to [myFilter] was [BackgroundImage] at 0
PASS: in2 from [BackgroundImage] to [myFilter] was [BackgroundImage] at 0.2
PASS: in2 from [BackgroundImage] to [myFilter] was [myFilter] at 0.6
PASS: in2 from [BackgroundImage] to [myFilter] was [myFilter] at 1

Web Animations API:
PASS: in from [SourceAlpha] to [FillPaint] was [SourceAlpha] at -0.4
PASS: in from [SourceAlpha] to [FillPaint] was [SourceAlpha] at 0
PASS: in from [SourceAlpha] to [FillPaint] was [SourceAlpha] at 0.2
PASS: in from [SourceAlpha] to [FillPaint] was [FillPaint] at 0.6
PASS: in from [SourceAlpha] to [FillPaint] was [FillPaint] at 1
PASS: in from [SourceAlpha] to [FillPaint] was [FillPaint] at 1.4
PASS: in2 from [BackgroundImage] to [myFilter] was [BackgroundImage] at -0.4
PASS: in2 from [BackgroundImage] to [myFilter] was [BackgroundImage] at 0
PASS: in2 from [BackgroundImage] to [myFilter] was [BackgroundImage] at 0.2
PASS: in2 from [BackgroundImage] to [myFilter] was [myFilter] at 0.6
PASS: in2 from [BackgroundImage] to [myFilter] was [myFilter] at 1
PASS: in2 from [BackgroundImage] to [myFilter] was [myFilter] at 1.4

